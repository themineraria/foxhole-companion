shader_type canvas_item;

uniform vec4 u_color_key : source_color;
uniform vec4 u_replacement_color : source_color;
uniform float tolerance : hint_range(0.0, 1.0, .05) = 0.1;

uniform float start_x:hint_range(-1.0, 1.0, 0.001) = 0.0;
uniform float end_x:hint_range(-1.0, 1.0, 0.001) = 1.0;

uniform float start_y:hint_range(-1.0, 1.0, 0.001) = 0.0;
uniform float end_y:hint_range(-1.0, 1.0, 0.001) = 1.0;

float chroma_key(vec4 color, vec4 chroma_key) {
	vec4 d4 = abs(color - chroma_key);
    return max(max(d4.r, d4.g), d4.b);
}

bool is_cropped(vec2 uv) {
	return !(uv.x>start_x && uv.x<end_x && uv.y>start_y && uv.y<end_y);
}

void fragment() {
	vec4 col = texture(TEXTURE, UV); // Get pixel color
	if (!is_cropped(UV)) { // If pixel is not cropped
		float diff = chroma_key(col, u_color_key); // Calculate the diff with the chroma key
    	if(diff < tolerance) { // If we need to change the color based on the chroma key
        	col = u_replacement_color;
    	}
	} else { // Pixel is cropped, full transparent
		col = vec4(0.0,0.0,0.0,0.0);
	}
    COLOR = col;
}