class_name Building
extends Sprite2D

var data = null
var building_name=null
var last_click_position=null
var dragged=false

static func create(d: Dictionary, scene_path = "res://Scenes/building.tscn") -> Building:
	var scene: PackedScene = load(scene_path)
	var building: Building = scene.instantiate()
	building.texture = load(d["sprite"])
	building.building_name=d["name"]
	building.add_to_group("building")
	return building
	
#vrai note connard : les building se font sateliser
#func _physics_process(delta):
	#scale = Vector2(get_node("/root/main/Map/Camera").max_zoom-1, get_node("/root/main/Map/Camera").max_zoom-1) - get_node("/root/main/Map/Camera").zoom
	#scale = scale.clamp(Vector2(0.2, 0.2), Vector2(1, 1))

func _process(_delta):
	if dragged && last_click_position != (get_local_mouse_position() + global_position): 
		position += get_local_mouse_position()

func _unhandled_input(event):
		
	if event.is_action_pressed("left_click") && get_rect().has_point(to_local(get_local_mouse_position() + global_position)):
		dragged=true
		get_tree().call_group("icon_layer","set_cursor",Control.CURSOR_DRAG)
		last_click_position = get_local_mouse_position() + global_position
		get_tree().get_root().set_input_as_handled()
		
	elif event.is_action_released("left_click") && last_click_position == (get_local_mouse_position() + global_position): 
		release_drag()
		get_tree().call_group("icon_layer","set_cursor",Control.CURSOR_ARROW)
		get_tree().call_group("building","open_panel",self)
		get_tree().get_root().set_input_as_handled()
		
func release_drag():
	dragged = false
	
	
