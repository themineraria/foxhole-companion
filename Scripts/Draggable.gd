class_name Draggable
extends Button

var building_name: String

static func create(building) -> Draggable:
	var scene: PackedScene = load("res://Scenes/draggable.tscn")
	var draggable: Draggable = scene.instantiate()
	draggable.icon=load(building.sprite)
	draggable.building_name=building.name
	return draggable

func _ready():
	add_to_group("draggable")

func _get_drag_data(at_position):
	set_drag_preview(_get_preview_control())
	return self

func _get_preview_control() -> Control:
	var preview = TextureRect.new()
	preview.texture = icon
	preview.set_rotation(.1)
	return preview
