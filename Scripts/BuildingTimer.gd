class_name BuildingTimer
extends Control

@onready var label = $Label
@onready var timer = $Timer

func _init():
	set_process_input(false)
	set_mouse_filter(Control.MOUSE_FILTER_PASS)
	
func _ready():
	timer.start()
	
func time_left_to_live():
	var time_left = timer.time_left
	var hour = floor(time_left / 3600)
	var minute = floor((int(time_left) % 3600) / 60)
	var second = int(time_left) % 60
	return [hour, minute, second]
	
func _process(delta):
	label.text = "%02d:%02d:%02d" % time_left_to_live()
