extends Panel
class_name DragZone

@export var tilemap: TileMap

func _init():
	set_process_input(false)
	set_mouse_filter(Control.MOUSE_FILTER_PASS)
	
func set_cursor(cursor: CursorShape):
	mouse_default_cursor_shape = cursor
	
func _ready():
	var tilemap_size = tilemap.get_used_rect()
	var tile_x_size = tilemap.tile_set.tile_size.x
	var tile_y_size = tilemap.tile_set.tile_size.y
	var tilemap_size_px=tilemap_size.size*Vector2i(tile_y_size,tile_x_size)
	size=tilemap_size_px
	anchors_preset=8
	position.x+=tile_x_size/2

func _can_drop_data(at_position, data) -> bool:
	var can_drop: bool=data is Node and data.is_in_group("draggable")
	return can_drop
	
func _drop_data(at_position, data):
	var building1 = create_building(StaticData.buildings[data.building_name], at_position.x, at_position.y)

func create_building(dictionary, x, y) -> Node:
	var building_instance=null
	match dictionary.name:
		"maintenance_tunnel" :
			building_instance = MaintenanceTunnel.create(dictionary, "res://Scenes/maintenance_tunnel.tscn")
		_:
			building_instance = Building.create(dictionary)
	building_instance.position = Vector2(x, y)
	add_child(building_instance)
	return building_instance

func _unhandled_input(event):
	if event.is_action_pressed("left_click"):
		get_tree().call_group("building","close_panel")
		get_tree().get_root().set_input_as_handled()
	elif event.is_action_released("left_click"):
		get_tree().call_group("building","release_drag")
		set_cursor(CURSOR_ARROW)
		get_tree().get_root().set_input_as_handled()

