extends VBoxContainer

func _ready():
	for building in StaticData.buildings:
		var draggable=Draggable.create(StaticData.buildings[building])
		add_child(draggable)
