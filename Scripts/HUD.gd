extends CanvasLayer

func _unhandled_input(event):
	if event.is_action_pressed("show_fps"):
		$"FPS meter".set("visible", !$"FPS meter".get("visible"))
		get_tree().get_root().set_input_as_handled()
