extends Node2D

@onready var free_camera = $Map/Camera

func _ready():
	free_camera.make_current()
