extends PanelContainer
var building: Building

func _ready():
	var close_button_texture=ImageTexture.create_from_image(Image.load_from_file("res://Images/HUD/close cross.webp"))
	close_button_texture.set_size_override(Vector2i(12,12))
	$"SpanningTableContainer/SpanningCellContainer/Close Button".icon=close_button_texture

func open_panel(node):
	if building != node:
		building = node
		visible=true
		print(node)

func close_panel():
	building=null
	visible=false

func _on_close_button_pressed():
	close_panel()

func _on_delete_button_pressed():
	building.queue_free()
	close_panel()
